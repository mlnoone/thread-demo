package com.sasyabook.threaddemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Thread.sleep
import java.util.concurrent.Executors

class MainActivity : AppCompatActivity() {

    private var keepRunning = true
    private var runProgress = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
    private fun setUIMessage(msg: String, buttonEnabled: Boolean) {
        runOnUiThread {
            textView.text = msg
            button.isEnabled = buttonEnabled
        }
    }
    fun startTask(view: View) {
        setUIMessage("Starting...", false)
        Log.d("THREAD", Thread.currentThread().name)
        Executors.newSingleThreadExecutor().submit(Runnable {
            while(runProgress < 100) {
                if (!keepRunning) {
                    setUIMessage("Interrupted..", true)
                    return@Runnable
                }
                runProgress ++
                runOnUiThread {
                    progressBar.progress = runProgress
                    val msg = "${runProgress}%"
                    textView.text = msg
                }
                Log.d("THREAD", Thread.currentThread().name)
                sleep(50)
            }
            setUIMessage("Done..", true)
        })
        /*
        Thread(Runnable {
            while(runProgress < 100) {
                    if (!keepRunning) {
                        setUIMessage("Interrupted..", true)
                        return@Runnable
                    }
                    runProgress ++
                    runOnUiThread {
                        progressBar.progress = runProgress
                        val msg = "${runProgress}%"
                        textView.text = msg
                    }
                    sleep(50)
                }
            setUIMessage("Done..", true)
        }).start()

         */
    }

    override fun onStop() {
        super.onStop()
        keepRunning = false
    }
}